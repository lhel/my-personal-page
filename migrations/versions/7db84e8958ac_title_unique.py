"""title unique

Revision ID: 7db84e8958ac
Revises: 325cba3edf74
Create Date: 2019-01-23 13:09:39.858262

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '7db84e8958ac'
down_revision = '325cba3edf74'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_unique_constraint(None, 'post', ['title'])
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_constraint(None, 'post', type_='unique')
    # ### end Alembic commands ###
