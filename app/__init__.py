from flask import Flask, Markup
from config import Config
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
import markdown

app = Flask(__name__)
app.config.from_object(Config)
db = SQLAlchemy(app)
migrate = Migrate(app, db)


@app.template_filter('markdown')
def get_markdown(s):
    return Markup(markdown.markdown(s))


from app import routes, models, commands
