from app.models import Post

from flask import render_template

from app import app


@app.route('/')
@app.route('/index')
def index():
    posts = Post.query.all()
    return render_template('index.html', posts=posts,
                           topics={p.topic for p in posts})


@app.route('/<title>')
def view_post(title):
    post = Post.query.filter_by(title=title.title().replace("-", " ")).first()
    print(post)
    return render_template('post.html', title=post.title, post=post)
