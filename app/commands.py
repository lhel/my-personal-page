from datetime import datetime

import click

from app import app, db
from app.models import Post


@app.cli.command()
@click.argument('title')
@click.argument('topic')
@click.argument('name_file')
def create_post(title, topic, name_file):
    content = open(name_file, 'r').read()
    post = Post(title=title.title(), topic=topic, timestamp=datetime.now(),
                author='lhel', content=content)
    db.session.add(post)
    db.session.commit()
    print("post saved: {}".format(repr(post)))
