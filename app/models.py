from datetime import datetime

from app import db


class Post(db.Model):
    """docstring for Post"""
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(40), unique=True)
    topic = db.Column(db.String(50))
    author = db.Column(db.String(50))
    timestamp = db.Column(db.DateTime, index=True, default=datetime.utcnow)
    content = db.Column(db.Text())

    def __repr__(self):
        return 'Post(title={}, topic={}, author={})'.format(self.title,
                                                            self.topic,
                                                            self.author)

    def __str__(self):
        return repr(self)
